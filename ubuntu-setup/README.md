# linux-tools

## Ubuntu Install

### Packages

```sh
# basic
sudo apt install -y zsh tree curl
# dev
sudo apt install -y git git-gui git-lfs python3-pip python3-distutils default-jre
# dev tools
sudo apt install -y ack meld plantuml
# software
sudo apt install -y terminator picocom
# snap software
sudo snap install flameshot ksnip
```

### Software

#### Shell
```sh
# set terminator default
sudo update-alternatives --config x-terminal-emulator
# set zsh default
chsh -s $(which zsh)
```

#### Powerline
```sh
sudo pip3 install powerline-status powerline-gitstatus
sudo apt-get install fonts-powerline powerline-gitstatus
```
Add this to `~/.zshrc.local`:
```zsh
# Use powerline in ZSH
powerline-daemon -q
source /usr/local/lib/python3.8/dist-packages/powerline/bindings/zsh/powerline.zsh

cp -r ./home/user/.config/powerline ~/.config/
```


#### Sublime
```sh
wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -
echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list
sudo apt-get update
sudo apt-get install -y sublime-text
```

#### VSCode
```sh
wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
sudo install -o root -g root -m 644 packages.microsoft.gpg /etc/apt/trusted.gpg.d/
sudo sh -c 'echo "deb [arch=amd64,arm64,armhf signed-by=/etc/apt/trusted.gpg.d/packages.microsoft.gpg] https://packages.microsoft.com/repos/code stable main" > /etc/apt/sources.list.d/vscode.list'
rm -f packages.microsoft.gpg
sudo apt install apt-transport-https
sudo apt update
sudo apt install code
```

### Configuration

#### FTDI
```sh
sudo usermod -aG dialout $(whoami)
# logout/login
```

#### Flameshot
Change screenshot application to Flameshot:
https://askubuntu.com/questions/1036473/how-to-change-screenshot-application-to-flameshot-on-ubuntu-18-04
Set path to: `/snap/bin/flameshot gui`
perhaps have to start using flameshot manually first (via launcher), before it works
