# bashrc for Windows git-bash

bind '"\e[A": history-search-backward'
bind '"\e[B": history-search-forward'

alias bashrc="vim ~/.bashrc && source ~/.bashrc"

alias l="ls -la"
alias subl="'/c/Program Files/Sublime Text/sublime_text.exe'"
export PATH=$PATH:"C:/Program\ Files/Meld"

meld() {
    start "C:/Program\ Files/Meld/Meld.exe" $@
}
