
# oh-my-zsh config
export ZSH="$HOME/.oh-my-zsh"
ZSH_THEME="robbyrussell"
plugins=(git)
source $ZSH/oh-my-zsh.sh

# useful `zshrc` alias
if [ -f ~/.zshrc.local ]; then
alias zshrc="vim ~/.zshrc.local && source ~/.zshrc.local"
source ~/.zshrc.local
fi
