#!/usr/bin/env sh

# A virtualenv helper script.
#
# Best to be made available on the system PATH, and/or best to use a shortcut or shell alias
# to help sourcing the script, e.g. add to the ~/.bashrc or ~/.zshrc file:
#
#     alias venv='source path/to/venv.sh
#
# The script helps to quickly create, activate and switch virtualenvs, and has functions to
# quickly install requirements.

PARSE_SUCCESS=255

THISFILE="$(basename "$0")"

print_help() {
    echo "Usage: venv.sh [<name>] [-r] [-i <dependencies>] [-I] [-h]"
    echo ""
    echo "DESC"
    echo "  Virtualenv helper. Needs to be sourced. Create/Recreate/Activate a given virtuelanv."
    echo "  Recommended to be used via a shell alias, e.g.:"
    echo "      alias venv='source venv.sh"
    echo ""
    echo "ARGS"
    echo "  <name>                   Name/path of the virtualenv. Default: .venv"
    echo ""
    echo "OPTIONS"
    echo "  -r, --recreate           Delete the virtualenv and re-create, if it already exists"
    echo "  -i, --install <file>     'pip install -r <file>' the given dependencies file"
    echo "  -I, --install-default    'pip install -r requirements.txt' (default dependencies file)"
    echo "  -v, --verbose            show python/pip/venv info after entering a virtualenv"
    echo "  -h, --help               show this help"
    echo ""
    echo "EXAMPLES"
    echo "  . ${THISFILE}"
    echo "  . ${THISFILE} -r"
    echo "  . ${THISFILE} .my-venv"
    echo "  . ${THISFILE} -i requirements-dev.txt"
    echo "  . ${THISFILE} -I"
    echo "  . ${THISFILE} -r -I .venv"
}

print_error() {
    printf "\e[0;31merror: %s\e[0m\n" "${1}"
}

print_warning() {
    printf "\e[0;33mwarning: %s\e[0m\n" "${1}"
}

cmd_line_parse() {

    unset VENV_NAME
    unset RECREATE_VENV
    unset DEPENDENCIES
    unset PRINT_INFO

    ARGS=$(getopt -l "recreate,install,install-default,help" -o "ri:Ivh" -- "$@")
    # shellcheck disable=SC2181
    if [ $? -ne 0 ]; then
        echo "---"
        print_help
        return 1
    fi

    eval set -- "$ARGS"

    while true; do
        ARG=${1}
        shift
        case "${ARG}" in
            -n | --name)
                VENV_NAME=${1}
                shift
                ;;
            -r | --recreate)
                RECREATE_VENV=2
                ;;
            -i | --install)
                DEPENDENCIES=${1}
                shift
                ;;
            -I | --install-default)
                DEPENDENCIES="requirements.txt"
                ;;
            -v | --verbose)
                PRINT_INFO=true
                ;;
            -h | --help)
                print_help
                return 0
                ;;
            --)
                break
                ;;
        esac
    done
    shift $((OPTIND - 1))

    VENV_NAME='.venv'
    if [ -n "${1}" ]; then
        VENV_NAME="${1}"
        shift
    fi

    if [ -n "${1}" ]; then
        print_error "invalid argument: ${1}"
        echo "---"
        print_help
        return 1
    fi

    return ${PARSE_SUCCESS}
}

# Determine whether the script is directly sourced
# (source: https://stackoverflow.com/a/28776166)
is_sourced() {
    if [ -n "$ZSH_VERSION" ]; then
        case $ZSH_EVAL_CONTEXT in *:file:*) return 0;; esac
    else  # Add additional POSIX-compatible shell names here, if needed.
        case ${0##*/} in dash|-dash|bash|-bash|ksh|-ksh|sh|-sh) return 0;; esac
    fi
    return 1
}

print_info() {
    if [ -z "${PRINT_INFO}" ]; then
        return 0
    fi
    echo "---- venv info ----"
    echo "venv:    ${VIRTUAL_ENV}" || return 1
    echo "python:  $(python --version)  ($(which python))" || return 1
    echo "pip:     $(pip --version)" || return 1
}

install_dependencies() {
    echo "installing dependencies: '${1}'"
    pip install --upgrade pip
    pip install -r "${1}"
}

require_program() {
    if ! command -v "${1}" >/dev/null ; then
        print_error "program '${1}' not found"
        return 1
    fi
}

if ! is_sourced ; then
    print_error "this script needs to be sourced: '. venv.sh'"
    exit 1
fi

cmd_line_parse "$@"
parse_result=$?
if [ "$parse_result" -ne "${PARSE_SUCCESS}" ]; then
    return $parse_result
fi

VENV_PATH="$(pwd)/${VENV_NAME}"

if [ -n "${DEPENDENCIES}" ] && [ ! -f "${DEPENDENCIES}" ]; then
    print_error "dependencies file '${DEPENDENCIES}' doesn't exist"
    return 1
fi

require_program "virtualenv" || return 1

if [ -n "${VIRTUAL_ENV}" ]; then
    if [ "${VIRTUAL_ENV}" = "${VENV_PATH}" ]; then
        if [ -z "${RECREATE_VENV}" ]; then
            echo "already in virtualenv '${VENV_NAME}'. Use the -r option to re-create"
            if [ -n "${DEPENDENCIES}" ]; then
                install_dependencies "${DEPENDENCIES}"
            fi
            print_info || return 1
            return 0
        fi
        echo "--recreate option: deactivate virtualenv"
        deactivate
    else
        print_warning "already in another virtualenv: '${VIRTUAL_ENV}'"
        echo "deactivating '$(basename "${VIRTUAL_ENV}")'"
        deactivate
    fi
fi
# not in an virtualenv from here

if [ -d "${VENV_PATH}" ] && [ -n "${RECREATE_VENV}" ]; then
    echo "--recreate option: delete existing virtualenv: ${VENV_PATH}"
    rm -rf "${VENV_PATH}"
fi

if [ ! -d "${VENV_PATH}" ]; then
    echo "creating and activating virtualenv '${VENV_NAME}': '${VENV_PATH}'"
    virtualenv "${VENV_PATH}" || return 1
    # shellcheck disable=SC1091
    . "${VENV_PATH}/bin/activate"
    echo "updating pip"
    pip install --upgrade pip
else
    echo "activating existing virtualenv: '${VENV_NAME}'"
    # shellcheck disable=SC1091
    . "${VENV_PATH}/bin/activate"
fi
# virtualenv exists and activated here

if [ -n "${DEPENDENCIES}" ]; then
    install_dependencies "${DEPENDENCIES}"
fi

print_info
